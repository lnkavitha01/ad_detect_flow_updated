from chalice import Chalice
import boto3
from boto3.dynamodb.conditions import Key

dynamodb_client = boto3.client('dynamodb',aws_access_key_id='AKIA3IHOK7RWNXVIMKOS',aws_secret_access_key='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',region_name='us-east-2')
table_name = 'streamntv-db'
existing_tables = dynamodb_client.list_tables()['TableNames']
if table_name not in existing_tables:
    create_table()
    
app = Chalice(app_name='streamntv-restapi')
dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table(table_name)


@app.route('/', methods=['GET'])
def index():
    response = table.scan()
    data = response.get('Items', None)

    return {'data': data}


@app.route('/item/{id}', methods=['GET'])
def item_get(id):
    response = table.query(
        KeyConditionExpression=Key("id").eq(id)
    )
    data = response.get('Items', None)

    return {'data': data}


@app.route('/item', methods=['POST'])
def item_set():
    data = app.current_request.json_body

    try:
        table.put_item(Item={
            "id": data['id'],
            "text": data['text']
        })

        return {'message': 'ok', 'status': 201, "id": data['id']}
    except Exception as e:
        return {'message': str(e)}

#Function to create dynamodb table with 'id' as HASH key and 'schedule' as the sort key

def create_table():
  
    import boto3
    dynamodb = boto3.resource('dynamodb',aws_access_key_id='AKIA3IHOK7RWNXVIMKOS',aws_secret_access_key='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',region_name='us-east-2')

    table = dynamodb.create_table(
    TableName='streamntv-db',
        KeySchema=[
        {
            'AttributeName': 'id',
            'KeyType': 'HASH'  #Partition key
        },
        {
            'AttributeName': 'schedule',
            'KeyType': 'RANGE'  #Sort key
        }
    ],
    AttributeDefinitions=[
        {
            'AttributeName': 'id',
            'AttributeType': 'N'
        },
        {
            'AttributeName': 'schedule',
            'AttributeType': 'S'
        }
       ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }
)

    print("Table status:", table.table_status)
