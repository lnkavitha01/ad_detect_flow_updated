# streamntv container | streamntv-container.tf
# container template

data "template_file" "streamntv_app" {
  template = file("./task_definition.json")
  vars = {
    app_name = var.streamntv_app_name
    app_image = var.streamntv_app_image
    app_port = var.streamntv_app_port
    fargate_cpu = var.streamntv_fargate_cpu
    fargate_memory = var.streamntv_fargate_memory
    aws_region = var.aws_region
  }
}

# ECS task definition
resource "aws_ecs_task_definition" "streamntv_app" {
  family = "streamntv-task"
  execution_role_arn = aws_iam_role.ecsTaskExecutionRole.arn
  network_mode = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu = var.streamntv_fargate_cpu
  memory = var.streamntv_fargate_memory
  container_definitions = data.template_file.streamntv_app.rendered
}
# ECS service
resource "aws_ecs_service" "streamntv_app" {
  name = var.streamntv_app_name
  cluster = aws_ecs_cluster.aws-ecs.id
  task_definition = aws_ecs_task_definition.streamntv_app.arn
  desired_count = var.streamntv_app_count
  launch_type = "FARGATE"
  network_configuration {
    security_groups = [aws_security_group.aws-ecs-tasks.id]
    subnets = aws_subnet.aws-subnet.*.id
    assign_public_ip = true
  }
  load_balancer {
    target_group_arn = aws_alb_target_group.streamntv_app.id
    container_name = var.streamntv_app_name
    container_port = var.streamntv_app_port
  }
  depends_on = [aws_alb_listener.front_end]
  tags = {
    Name = "${var.streamntv_app_name}-ecs"
  }
}