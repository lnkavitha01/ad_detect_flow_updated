#!/bin/bash

j=7

myArr[7]='MTVC'
myArr[8]='TRU-TV'

raf=('233.14.186.162'
'233.14.186.105'
)

for i in "${raf[@]}"
do
#get service status
http_status_out=`curl -I  --stderr /dev/null 127.0.0.${j}:8080 | head -1 | cut -d' ' -f2`

http_header_ok=200
if [ "$http_status_out" == "$http_header_ok" ]; then
	echo "Service running for 127.0.0.${j}:8080"
else
#start service note 
	nohup vlc -vvv -I dummy  --miface=ens192 udp://@${i}:8208 --sout "#http{mux=ts,dst=127.0.0.${j}:8080}" --file-logging --logfile=/home/streamn/streamn/logs/vlc-log_${myArr[j]}.txt --sout-file-format &
fi
      j=$((j+1))
done

