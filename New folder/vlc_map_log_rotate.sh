#!/bin/bash
PRE_DAY=$(date -d "1 day ago"  +"%d%m%Y")
OLD15_DAYS=$(date -d "15 day ago" +"%d%m%Y")

channels=(MTVC
TRU-TV
)

cd /home/streamn/streamn/logs
for i in "${channels[@]}"
do

cp vlc-log_${i}.txt vlc-log_${i}_$PRE_DAY.txt
> vlc-log_${i}.txt

rm -f vlc-log_${i}_$OLD15_DAYS.txt

done
