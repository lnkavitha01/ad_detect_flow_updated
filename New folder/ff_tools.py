import six
import subprocess
import os
import json
import time

FFPROBE_PATH = "ffprobe"

def probe(fname):
    # def is_url(url):
    #     #: Accept objects that have string representations.
    #     try:
    #         url = unicode(url)
    #     except NameError:
    #         # We're on Python 3.
    #         url = str(url)
    #     except UnicodeDecodeError:
    #         pass
    # if not os.path.exists(fname) and not is_url(fname):
    #     return None

    print("ffprobe command: {}".format(" ".join(
        [FFPROBE_PATH, '-v', 'quiet', '-print_format', 'json', '-show_format', '-show_streams', '-show_programs',
         fname])))
    p = subprocess.Popen([FFPROBE_PATH, '-v', 'quiet', '-print_format', 'json', '-show_format', '-show_streams', '-show_programs', fname], shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
    # special case RTMP protocol since ffmpeg does not return if the stream isn't present
    if fname.startswith(("rtmp://", "rtmps://")):
        secs_to_wait = 10
        while p.poll() is None:
            if secs_to_wait > 0:
                time.sleep(1.0)
                secs_to_wait -= 1
            else:
                p.terminate()
                return None
    stdout_data, _ = p.communicate()
    stdout_data = six.ensure_str(stdout_data, 'ascii')
    info = json.loads(stdout_data)

    info['posters'] = []
    if 'streams' in info:
        for stream in info['streams']:
            try:
                attached_pic = stream['disposition']['attached_pic']
            except KeyError:
                attached_pic = None
            try:
                if stream['codec_type'] not in info:
                    info[stream['codec_type'] + 's'] = []
                    if (stream['codec_type'] != 'video' or not attached_pic):
                        info[stream['codec_type']] = stream
                info[stream['codec_type'] + 's'].append(stream)
            except:
                pass
            if attached_pic:
                info['posters'].append(stream)

    def de_under(key, value, branch):
        branch[key.replace('_', '')] = value

    def lower(key, value, branch):
        branch[key] = value.lower()

    def container(key, value, branch):
        info['container'] = value.lower().split(',')

    def extension(key, value, branch):
        info['extension'] = os.path.splitext(value)[1][1:].lower()

    def codec(key, value, branch):
        if 'aac' in value:
            value = 'aac'
        branch['codec'] = value.lower()

    def bitrate_in_kbps(key, value, branch):
        branch['bitrate'] = round(value / 1000.0)

    def bitrate_in_mbps(key, value, branch):
        branch['bitrate'] = round(value / 1000.0 / 1000, 1)

    def fps(key, value, branch):
        branch['fps'] = round(value, 2)

    def level(key, value, branch):
        branch[key] = round(value / 10.0, 1)

    FIELD_MAPPING = {
        'format': {
            'format_name': container,
            'filename': extension,
            'bit_rate': bitrate_in_mbps,
        },
        'audios': {
            'codec_name': codec,
            'sample_rate': de_under,
            'bit_rate': bitrate_in_kbps,
        },
        'videos': {
            'codec_name': codec,
            'avg_frame_rate': fps,
            'bit_rate': bitrate_in_mbps,
            'profile': lower,
            'level': level,
        }
    }

    def clean_info(branch, parent=None):
        if parent is None:
            mapping = FIELD_MAPPING
        else:
            mapping = FIELD_MAPPING.get(parent, {})

        # branch is modified in this for loop, so a list copy must be made of the items
        for key, value in list(branch.items()):
            if key in ('streams', 'audio', 'video'):
                continue

            if isinstance(value, dict):
                clean_info(value, key)
                continue

            if isinstance(value, list):
                for item in value:
                    clean_info(item, key)
                continue

            if isinstance(value, six.string_types):
                try:
                    if '/' in value:
                        n, d = value.split('/', 1)
                        value = float(n) / float(d)
                    elif '.' in value:
                        value = float(value)
                    else:
                        value = int(value)
                except ZeroDivisionError:
                    value = 0
                except ValueError:
                    value = value.strip()
                branch[key] = value

            if key in mapping:
                mapping[key](key, value, branch)

    clean_info(info)

    return info
