import os
import sys
import time
import threading
import logging
from datetime import datetime
import subprocess
import psutil
import ff_tools
import six
import math
import re
import argparse
from urllib.parse import urlparse


logger = logging.getLogger(__name__)
ffmpeg_bin = 'ffmpeg'
ffprobe_bin = 'ffprobe'


class StreamNotAvailableException(Exception):
    pass


sbr1_ladder = [{ 'height': 480, 'width': 720, 'fps_min': 0, 'fps_max': 60, 'video_bitrate': '3400k' }]

# TODO: pick the right height from ffprobe intially and set here. sbr1_ladder hxw will also have to be changed based on probe result
# TODO: the bitrate is right for this range.
preferred_height = 480


def run_freq(f):
    def wrapper(*args, **kwargs):
        if  time.time() - wrapper.last_run > 60 // 2:
            wrapper.last_run = time.time()
            return f(*args, **kwargs)
        else:
            return True
    wrapper.last_run = time.time()
    return wrapper


class Video(object):
    def __init__(self):
        # internal control flow objects
        self.dir_name = None
        self.defaultPath = '/Users/ggovindan/' # TODO: this path needs to be changed ot the actual dir where the ts needs to go
        self.ffmpeg_report_path = None
        self.p = None
        self.ffmpegPid = None
        self.ffmpeg_ended = False
        self.ffDuration = None
        self.interlaced = False
        self.transcode = True
        self.pathIn = None
        self.network_node = None
        self.ts_segment_prefix = None
        self.start_dt = None
        # frame timing objects
        self.fps = 30.0
        self.worker_thread = None
        self.num_ffmpeg_restarts = 0

    def __str__(self):
        attrs = vars(self)
        return ", ".join("%s: %s" % item for item in attrs.items())

    @staticmethod
    def OpenVideo(pathIn, dir_name, start_dt=None, ts_segment_prefix=None, default_path="/var/admon/"):
        video = Video()
        video.defaultPath = default_path
        video.network_node = ts_segment_prefix.split("_", 1)[0]
        video.ts_segment_prefix = ts_segment_prefix
        video.start_dt=start_dt

        # TODO: setup logging

        video.dir_name = dir_name

        # experts recommend using nonfatal option to avoid circular buffer overrun for streams over UDP
        if pathIn.startswith("udp://"):
            pathIn = pathIn 
            #+ "?timeout=4000000&overrun_nonfatal=1&fifo_size=270000"  # add 4 sec timeout in case HomeRun stream pauses
        # TODO: If we are dealing with other stream like m3u8 it needs to be handled here
        video.pathIn = pathIn
        try:
            video.get_details(pathIn)
        except Exception as e:
            print('get_details failed: ' + str(sys.exc_info()[1]))
            raise StreamNotAvailableException("Probing the stream {} failed for: {}".format(pathIn, e))
        if not os.path.exists(video.defaultPath):
            os.makedirs(video.defaultPath)
        try:
            video.launch_ffmpeg(pathIn, video.dir_name)
        except:
            logger.error('launch_ffmpeg failed: ' + str(sys.exc_info()[1]))
            raise StreamNotAvailableException("Launching ffmpeg failed.")
        video.worker_thread = threading.Thread(target=video.health_check, args=())
        video.worker_thread.daemon = True
        video.worker_thread.start()
        return video

    def get_details(self, pathIn):
        video_info = ff_tools.probe(pathIn)
        if video_info is None:
            raise StreamNotAvailableException("unable to run ffprobe on stream")
        self.ffDuration = video_info['format'].get('duration', 0.0)
        self.fps = video_info['video']['avg_frame_rate']
        if self.fps == 0:
            self.fps = video_info['video']['r_frame_rate']
        if self.fps > 30:
            # TODO: set a warning here
            print("higher fps: {}".format(self.fps))
        # determine if source content is interlaced
        # this is not needed if the stream type is HLS
        params = [
            ffmpeg_bin,
            "-hide_banner",
            "-filter:v", "idet",
            "-frames:v", "%d" % math.ceil(self.fps * 5),
            "-an", "-f", "rawvideo", "-y", "/dev/null",
            "-i", pathIn
        ]
        # p = subprocess.Popen(params, shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
        # stdout_lines, stderr_lines = p.communicate()
        # stderr_lines = six.ensure_str(stderr_lines, encoding='ascii').splitlines()
        # ti = tp = 0
        # for line in stderr_lines:
        #     if 'frame detection:' in line:
        #         counts = re.findall(' \\d+ ', line)
        #         ti += int(counts[0]) + int(counts[1])
        #         tp += int(counts[2])
        # self.interlaced = False if tp >= ti * 3 else True

    def launch_ffmpeg(self, pathIn, dir_name):
        self.dir_name = dir_name
        ffmpeg_running = self.ffmpeg_already_running()
        if ffmpeg_running:
            print("ffmpeg already running for this stream!")
            return

        fps = str(int(round(self.fps)))
        x264opts = 'keyint=' + fps + ':min-keyint=' + fps + ':no-scenecut'
        ladder = sbr1_ladder
        manifest_fmt = "{prefix:s}.m3u8"
        output_folder_fmt = "{prefix:s}"

        env = os.environ.copy()
        ip_port = urlparse(pathIn).netloc
        ip_port = ip_port.replace(".", "_").replace(":", "_")
        self.ffmpeg_report_path = os.path.join(self.defaultPath, '%s' % (ip_port))
        params = [
            ffmpeg_bin,
            "-i", pathIn,
        ]
        # if self.interlaced:
        #     params.extend(["-vf", "yadif"])
        for rung in ladder:
            if rung['height'] == preferred_height and rung['fps_min'] <= int(fps) <= rung['fps_max']:
                manifest_path = manifest_fmt.format(prefix=os.path.join(self.defaultPath, self.dir_name), height=rung['height'])
                print('*'*70 , manifest_path  , 'manifest_path')
                self.add_an_output(params, x264opts, rung['width'], rung['height'], rung['video_bitrate'],
                                    output_folder_fmt.format(prefix=os.path.join(self.defaultPath, self.dir_name), height=rung['height']),
                                    manifest_path)

        if not ffmpeg_running:
            print("launching ffmpeg: {}".format(" ".join(params)))
            self.p = subprocess.Popen(params, env=env, stdout=open(os.devnull, 'wb'),
                                      stderr=open(os.devnull, 'wb'), close_fds=True)
            self.ffmpegPid = self.p.pid
            print("spawned %s: %s" % (self.ffmpegPid, params))


    def ffmpeg_already_running(self):
        ffmpeg_args = argparse.ArgumentParser(add_help=False)
        ffmpeg_args.add_argument('-i', dest='path_in', default=None)

        for proc in psutil.process_iter():
            try:
                pinfo = proc.as_dict(attrs=['pid', 'name', 'cmdline'])
            except psutil.NoSuchProcess:
                pass
            else:
                if pinfo['cmdline'] is None or "ffmpeg" not in " ".join(pinfo['cmdline']):
                    continue
                args, _ = ffmpeg_args.parse_known_args(pinfo['cmdline'][1:])
                if args.path_in == self.pathIn:
                    # already running
                    return True
        return False

    @run_freq
    def is_ffmpeg_running(self):
        is_running = self.ffmpeg_already_running()
        print("ffmpeg_health_check running={}".format(is_running))
        return is_running



    def add_an_output(self, params, x264opts, w, h, vb, output_folder, output_filename):
        # params.extend(["-ac", "2"])
        # params.extend(["-b:a", "128k"])
        # params.extend(["-c:v", "libx264"])
        # params.extend(["-b:v", vb])
        # params.extend(["-x264opts", x264opts])
        # params.extend(["-profile:v", "high"])
        # params.extend(["-s", str(w) + 'x' + str(h)])
        params.extend(["-c:v", "copy"])
        params.extend(["-c:a", 'copy'])
        params.extend(["-f", "hls"])
        params.extend(["-hls_flags", "program_date_time"])
        params.extend(["-hls_time", "10"])
        params.extend(["-hls_list_size", str(0)])
        params.extend(["-hls_base_url", output_folder + "/"])
        params.extend(["-strftime" ,"1"])
        params.extend(["-hls_flags" ,"second_level_segment_index"])
        hls_segment_filename = output_folder+f"/{self.ts_segment_prefix}_%H%M%S_%%05d.ts"
        params.extend(["-hls_segment_filename", hls_segment_filename])
        params.append(output_filename)

        # make sure output folder exists and output manifest does not
        target_folder = os.path.join(self.defaultPath, output_folder)
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        try:
            os.remove(output_filename)
        except OSError:
            pass

    def restart_ffmpeg(self):
        self.p.kill()
        time.sleep(10)
        self.start_dt = datetime.utcnow()
        
        self.ts_segment_prefix = "_".join([self.network_node, self.start_dt.strftime("%Y%m%d")])
        self.dir_name = self.start_dt.strftime("%Y%m%d")
        # print("Restarting FFMPEG",  datetime.utcnow().minute , self.start_dt.minute, self.dir_name)
        ##save the new m3u8 file info in the trackker csv
        self.ts_segment_path = os.path.join(self.network_node.strip() , f'{self.start_dt.strftime("%Y%m%d")}.m3u8')
        channel_config_file = f"/var/admon/channels_config/{self.network_node.strip()}_config.csv"
        df = pd.read_csv(channel_config_file)
        if df[df.Filename == self.ts_segment_path].shape[0] == 0:
            df.loc[len(df)] = [self.ts_segment_path , 0 , None ]
            df.to_csv(channel_config_file , index = False)
        
        self.launch_ffmpeg(self.pathIn, self.dir_name)

        


    def health_check(self):
        while True:
            try:
                if not self.is_ffmpeg_running():
                    # create a new TS Segment extension
                    ts_segment_prefix = "_".join([self.network_node, datetime.utcnow().strftime("%Y%m%d")])
                    self.num_ffmpeg_restarts += 1
                    print(
                        "launching new ffmpeg self.pathIn={}, self.num_ffmpeg_restarts={}".format(
                            self.pathIn, self.num_ffmpeg_restarts))
                    # NOTE: while this will allow us to access the new stream. the continuation of the stream will be broken
                    # If someone tries to run the stream from the beggiging will have issues as it will run into PTS issues
                    # unless the m3u8 is fixed with discontinuity tag
                    self.launch_ffmpeg(self.pathIn, self.dir_name)

                
                if datetime.utcnow().day != self.start_dt.day:
                    self.restart_ffmpeg()
            except Exception as e:
                print("Exception in health check e:{}".format(e))
            time.sleep(10)

import pandas as pd

if __name__ == "__main__":
    # assuming the entries in the txt file will be in this format
    # udp://233.12.34.45:8208,ESPN
    # udp://233.12.34.145:8208,CNN
    video_objects = []
    channel_config_dir = "/var/admon/channels_config"
    if not os.path.exists(channel_config_dir):
        os.makedirs(channel_config_dir)

    with open("video_ingest_config.txt", "r") as f:
        for line in f:
            path_in, network_node = line.split(",")
            start_dt = datetime.utcnow()
            ts_segment_prefix = "_".join([network_node.strip(), start_dt.strftime("%Y%m%d")])
            ts_segment_path = os.path.join(network_node.strip() , f'{start_dt.strftime("%Y%m%d")}.m3u8')
            
            channel_config_file = os.path.join(channel_config_dir, f"{network_node.strip()}_config.csv")
            if os.path.exists(channel_config_file):
                df = pd.read_csv(channel_config_file)
                if df[df.Filename == ts_segment_path].shape[0] == 0:
                    df.loc[len(df)] = [ts_segment_path , 0 , None ]
                    df.to_csv(channel_config_file , index = False)
            else:
                df = pd.DataFrame(columns=['Filename' , 'Processed' , 'LastProcessedFile'])
                df.loc[len(df)] = [ts_segment_path , 0 , None ]
                df.to_csv(channel_config_file , index = False)

            dir_name = start_dt.strftime("%Y%m%d")
            video_obj = Video.OpenVideo(path_in.strip(), dir_name, start_dt=start_dt, ts_segment_prefix=ts_segment_prefix, default_path=f"/var/admon/TS/{network_node.strip()}")
            video_objects.append(video_obj)

    while True:
        time.sleep(10)