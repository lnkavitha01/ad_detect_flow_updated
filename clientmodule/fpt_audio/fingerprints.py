'''
    This file is written as a kind of an entry point. c++ will call this file and executes the function
    written here.This file will call main.py for audio and video fingerprinting.
'''
import os
import time
import multiprocessing
import glob
from datetime import datetime
import logging
import pandas as pd
import main
import traceback

def fingerprinting(base_path, channel, ftype, field_order=None):
    '''
        arguments:-
            base_path: base path for input and output. Right we are using /var/admon as base path.
            channel: Channel Name
            ftype: pass audio for audio fingerprinting and video for video fingerprinting.
            field_order: this will tell if video is progresssive or interlaced.
        retuns:
            This will not return anything,
        function:-
            Use to call analyse_audio and analyse_video inside main.py
    '''
    logger = logging.getLogger(f"{channel}_{ftype}")
    try:
        channel_config_df = pd.read_csv(os.path.join(base_path, f"channels_config/{channel}_config.csv"))
        filenames = channel_config_df[(channel_config_df.Processed == 0)]["Filename"].tolist()
        processed_files_counter = 0
        threshold_wait = 30
        prev_hash = None
        fcounter = 0
        total_files = len(filenames)
        if ftype == "audio":
            fdir = "AUDIO_FEAT"
        else:
            fdir = "VIDEO_FEAT"

        while fcounter < total_files:
            filename = filenames[fcounter]
            folder_name  = filename.split('.')[0].split('/')[-1]
            folder_path = f'{base_path}/{fdir}/{channel}/{folder_name}'
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)
            all_files = glob.glob(f'{folder_path}/*.tsv')
            
            already_processed = []
            for pfile in all_files:
                fname = pfile.split('.')[0].split('/')[-1]
                already_processed.append(fname)

            ts_file = open(f'{base_path}/TS/{filename}',  'r')
            waited = 0
            
            while 1:
                line = ts_file.readline()
                if (line=='') and (waited == 0):
                    waited = 1
                    time.sleep(threshold_wait)
                    ts_file = open(f'{base_path}/TS/{filename}',  'r')
                    continue
                elif(line == '') and (waited == 1):
                    channel_config_df = pd.read_csv(os.path.join(base_path, f"channels_config/{channel}_config.csv"))
                    filenames = channel_config_df[(channel_config_df.Processed == 0)]["Filename"].tolist()
                    total_files = len(filenames)
                    fcounter += 1
                    break

                
                if '#' not in line:
                    rfile = line.replace('\n', '')
                    fname = rfile.split('.')[0].split('/')[-1]
                    dateandtime = ''.join(fname.split("_")[1:3])
                    #initial_start_time = round(datetime.strptime(dateandtime, "%Y%m%d%H%M%S").timestamp()*1000)
                    if not os.path.exists(rfile):
                        print ("exiting fingerprints for this file", rfile)
                        #this breaks for this day; to check
                        processed_files_counter = 1
                        already_processed.append(fname)
                        continue
                    if fname not in already_processed:
                        waited = 0
                        if ftype == "audio":
                            if processed_files_counter == 0:
                                dateandtime = ''.join(fname.split("_")[1:3])
                                initial_start_time = round(datetime.strptime(dateandtime, "%Y%m%d%H%M%S").timestamp()*1000)
                                logger.debug(f"Starting audio fingerprinting with initial time: {initial_start_time}")
                                prev_data, time_saved, prev_video_duration = main.analyse_audio(rfile, fname , folder_path, is_start = True, saved_buffer= None, is_split= True, timestamp_start_time= initial_start_time)
                                next_start_time = initial_start_time + prev_video_duration*1000
                            else:
                                dateandtime = ''.join(fname.split("_")[1:3])
                                actual_start_time = round(datetime.strptime(dateandtime, "%Y%m%d%H%M%S").timestamp()*1000)
                                if not 'initial_start_time' in locals() :
                                    dateandtime = ''.join(fname.split("_")[1:3])
                                    initial_start_time = round(datetime.strptime(dateandtime, "%Y%m%d%H%M%S").timestamp()*1000)
                                if not 'next_start_time' in locals() :
                                    next_start_time = initial_start_time
                                
                                if(abs(next_start_time -actual_start_time)/(1000)> 5):
                                    logger.warning(f"Time difference is greater than 5sec: {fname}")
                                    logger.warning(f"previous_video_duration {prev_video_duration}, next_start _time {next_start_time}, actual_time {actual_start_time}")
                                    next_start_time = actual_start_time
                                    prev_data, time_saved, prev_video_duration  = main.analyse_audio(rfile, fname , folder_path, is_start = True, saved_buffer= None, timestamp_start_time= next_start_time , is_split= True)
                                else:
                                    if not 'initial_start_time' in locals() :
                                        dateandtime = ''.join(fname.split("_")[1:3])
                                        initial_start_time = round(datetime.strptime(dateandtime, "%Y%m%d%H%M%S").timestamp()*1000)
                                    if not 'next_start_time' in locals() :
                                        next_start_time = initial_start_time
                                    if not 'prev_data' in locals() :
                                        prev_data, time_saved, prev_video_duration = main.analyse_audio(rfile, fname , folder_path, is_start = True, saved_buffer= None, is_split= True, timestamp_start_time= initial_start_time)
                                    else:    
                                        prev_data, time_saved, prev_video_duration  = main.analyse_audio(rfile, fname , folder_path, is_start = False, saved_buffer= prev_data, timestamp_start_time= next_start_time-400 , is_split= True)
                                
                                next_start_time += prev_video_duration*1000
                        else:
                            dateandtime = ''.join(fname.split("_")[1:3])
                            initial_start_time = round(datetime.strptime(dateandtime, "%Y%m%d%H%M%S").timestamp()*1000)
                            prev_hash = main.analyse_video(rfile, fname, folder_path, initial_start_time, prev_hv = prev_hash, field_order = field_order)
                        
                        processed_files_counter = 1

                        already_processed.append(fname)
            ts_file.close()
    except Exception as error:
        logger.error("Exception occured", exc_info=True)
        print(error)
        traceback.print_exc()


def generate_fingerprints_from_stream(main_folder, input_type):
    network_info_file = "network_info_with_field_order.csv"  
    ntw_info_df = pd.read_csv(os.path.join(main_folder, network_info_file))

    channel_config_dir = os.path.join(main_folder, "channels_config")
    #all_channels = [filename.split('/')[-1].split('_')[0].strip() for filename in glob.glob(f'{channel_config_dir}/*.csv')][0:2]
    all_channels = ["BLOOM" , "CENT", "MTVC" , "TRU-TV"]
    print("Channels Running", all_channels)
    logs_dir = os.path.join(main_folder , "logs")
    if not os.path.exists(logs_dir):
        os.makedirs(logs_dir)

    thread_list = []
    for channel in all_channels:
        field_order = ntw_info_df[ntw_info_df["network_code"] == channel]["field_order"].values[0]
        print("Starting Process....")
        
        logger = logging.getLogger(f"{channel}_{input_type}")
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler(f"{logs_dir}/{channel}_{input_type}.log")
        fh.setLevel(logging.DEBUG)
        fomatter=logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(fomatter)
        logger.addHandler(fh)
        logger.info("Starting fingerprinting")
        try:
            if input_type == "audio":
                # fingerprinting(main_folder, channel, input_type)
                thread_audio = multiprocessing.Process(target=fingerprinting, args=(main_folder, channel, input_type))
                thread_audio.start()
                thread_list.append(thread_audio)
            else:
                thread_video = multiprocessing.Process(target=fingerprinting, args=(main_folder, channel, input_type, field_order))
                thread_video.start()
                thread_list.append(thread_video)
        except Exception as error:
            logger.error("Exception occured:", exc_info=True)

    for thread_ in thread_list:
        thread_.join() 
   


def generate_fingerprints_from_file(input_folder, config_file):
    complete_file_path = os.path.join(input_folder, config_file)
    
    start_time = time.time()
    
    audio_output_folder = f'{input_folder}/output/audio_fingerprints'
    video_output_folder = f'{input_folder}/output/video_fingerprints'
    
    if not os.path.exists(audio_output_folder):
        os.makedirs(audio_output_folder)
    
    if not os.path.exists(video_output_folder):
        os.makedirs(video_output_folder)

    with open(complete_file_path, "r") as f:
        for line in f:
            line = line.replace('\n', '')        
            filename  = os.path.join(input_folder, f'input/{line}')
            name = filename.split('.')[0].split('/')[-1]
            
            main.analyse_audio(filename, name , audio_output_folder, is_start = True, saved_buffer = None, is_split= True)
            main.analyse_video(filename, name, video_output_folder)
       
    print("--- %s seconds ---" % (time.time() - start_time))



if __name__ == "__main__":
    try:
        generate_fingerprints_from_stream("/var/admon", "audio")
    except:
        traceback.print_exc()
