// Audio fingerprinting main()
 
#include <stdio.h>
// #include <stdint.h>
#include <iostream>
#include <string>
#include <math.h>
#include <Python.h>
#include <numpy/npy_common.h>
#include <numpy/arrayobject.h>
#include "pyhelper.hpp"

#define __FPT_DEBUG
#define __FPT_ENABLE

using namespace std;


int main(int argc, char **argv)
{
  string filename;
  string wpath;
  string input_folder_path, trackker_file_rpath;
  string input_medium; // this can be stream and file.
 
  
  cout<<"Starting Fingerprinting..."<<endl;
  CPyInstance hInstance;
  
  CPyObject pName =  PyUnicode_FromString("fingerprints");
  CPyObject pModule = PyImport_Import(pName);

  if(!pName){
    cout<<"Pname is not able to load to load the module"<<endl;
  }

  if(!pModule){
    cout<<"Not able to load fingerprint module"<<endl;
  }


  input_medium = argv[1];
  
  if (input_medium == "file"){ 
    //This has not been tested for long time. 
    input_folder_path = argv[2];
    trackker_file_rpath = argv[3];
    CPyObject pFunc = PyObject_GetAttrString(pModule, "generate_fingerprints_from_file");
    CPyObject pValue = PyObject_CallFunction(pFunc, "ss", input_folder_path.c_str() , trackker_file_rpath.c_str());
  }
  else if (input_medium == "stream"){
    //This is used to generate fingerprints for TS files which are captured using video ingestion process.
    cout<<input_medium<<endl;
    CPyObject pFunc = PyObject_GetAttrString(pModule, "generate_fingerprints_from_stream");
    CPyObject pValue = PyObject_CallFunction(pFunc , "ss" , argv[2], argv[3]);
  }
 }
