# main.py
from libraries import *
from utils import *
from collections import defaultdict
import warnings
from threading import Thread
import six
import time
import glob
import json
import multiprocessing
import logging
# function to genarate video fingerprints
def analyse_video(file, name, wpath, time_start,  prev_hv = None, regions=[], max_qlength=30, ref=1, ID=None, save_img=1, DEBUG=0, field_order = "progressive"):
    """
    arguments:-
        file: Path of the file to process
        name: Name of the file (used to for naming output files)
        wpath: Output Directory to write the frames,parquet and tsv files
        regions: 2d array with region cordiantes to calculate fingerprints
        max_qlength: max window length for ring buffer(Doubt)
        Id: A unique number given to Identify files
        save_img: Boolean
                    True(1): saves the frames to folder(wpath)
                    False(0):Doesn't save the frames
        ref: Boolean
                  Note:used only if save_img=True
                    True(1): saves the frames with timestamp in the name of frame
                    False(0): saves the image with name by adding timestamp and current time
        DEBUG: Boolean
                  True: Prints data and saves files
                  False: Just saves the files
    returns:
        None
    function:-
      Generates the Video fingerprints by using the "Calcs" function, also calls "write_parquet"  to save the output to files
    """
    try:
        video_start_time = time.time()
        # remove the above line if you want to pass Id seperately
        ID = name.split('_')[0]
        if ID == None:
            ID = random.getrandbits(32)

            # for frinting video on multiple regions(max 2 supported )
        # detect how many different frames regions are listed in regions
        crop_original = 0
        crop_region = []
        if len(regions) == 1:
            crop_original = 1
        elif len(regions) == 2:
            crop_original = 1
            crop_region = regions[1]
        elif len(regions) == 0:
            crop_original = 0
        else:
            return "more than two regions feature is not added yet,please check regions parameter"
        if crop_original:
            if len(regions[0]) == 4 or (len(regions) > 1 and len(regions[1]) == 4):
                pass
            else:
                return "please pass correct measures in regions parameter"

        # circular buffer above is used typically in streaming applications
        frames = None
        pdq_hashes = None
        qualities = None
        timestamps = []
        cnt = 0
        # for arg in argv:
        cap = cv2.VideoCapture(file)

        width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        channels = cap.get(cv2.CAP_PROP_CHANNEL)
        image_size = int(width) * int(height) * int(channels)
        buff = RingBuffer(capacity=max_qlength, dtype=np.object)  # intializing a ringbuffer
        # prev_hv = None

        pdq_type = []
        pdq_change = []
        frames = []
        frames_img = []
        pdq_hashes = []
        qualities = []
        timestamps = []
        region_type = []
        frame_class = []
        last_frames_size = 0
        frames_size = 0
        fps = cap.get(cv2.CAP_PROP_FPS)  # frames for second
        # time_start = cap.get(cv2.CAP_PROP_POS_MSEC)
        total = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))  # total Number of frames
        # print ("total frames", total)
        try:
            while (cap.isOpened()):
                ret, frame = cap.read()
                if(field_order != 'progressive'):
                    frame = np.delete(frame, range(1, frame.shape[1], 2), axis=1)
                else:
                    frame = np.delete(frame, range(1, frame.shape[0], 2), axis=0)
                    frame = np.delete(frame, range(1, frame.shape[1], 2), axis=1)
                ##change frame here-> intelaced
                frame_orig = frame
                # if we have multiple regions taking the first one
                if crop_original == 1:
                    # select original / main region
                    frame_orig = frame[regions[0][0]:regions[0][0] + regions[0][2],
                                regions[0][1]:regions[0][1] + regions[0][3]]

                # maintain a count for recent frames of size sliding_window
                # ie evaluate min and max PDQ diff. for frames in every sliding_window group
                # if DEBUG == 1:
                # print ("len buff", len(buff))
                # if cnt%50==0:
                # print(frame_orig.shape)
                # cv2_imshow(frame_orig)

                if DEBUG and cnt >= total - 1:
                    print("cnt", cnt)
                if len(buff) >= sliding_win or (cnt >= total - 1 and cnt <= total + 1):
                    if DEBUG and len(buff) != sliding_win:
                        print("cnt", cnt)
                    buff_temp = copy.deepcopy(buff)
                    prev_hv = calcs(buff_temp, buff, cap, ID, cnt - sliding_win, sliding_win, frames, pdq_hashes, qualities,
                                    pdq_type, pdq_change,
                                    crop_region, frame_class, region_type, prev_hv)

                    frames_size = len(frames)
                    # print("len frames, start_fr", frames_size, cnt-sliding_win)

                    for pos in range(last_frames_size, frames_size):
                        frames_img.append(buff_temp[frames[pos] - (cnt - sliding_win)])
                        timestamps.append(int(time_start + frames[pos] * 1000 / float(fps)))
                        # print ("frame num, timestamp", frames[pos] , int(time_start + frames[pos] * 1000/float(fps)) )

                    while (len(buff) > 0):
                        buff.popleft()

                    # save. new frame after processing previous of size sliding_win
                    # use cropped frame_orig instead of frame
                    save_frame_ring(buff, frame_orig, max_qlen)
                    last_frames_size = frames_size

                else:
                    # print ("E.len buff", len(buff))
                    save_frame_ring(buff, frame_orig, max_qlen)
                    # print ("E2. len buff", len(buff))

                if cnt >= total:
                    break
                cnt += 1
        except TypeError:
            pass
        except Exception as err:
            print(err)
        # write parquet for. each region type
        # print("lenght of \npdq_hashes",len(pdq_hashes),"\ntimestamps",len(timestamps),"\npdq type",len(pdq_type),"\nframe_class",len(frame_class),"\nregion_type",len(region_type))
        # write the output to files and saving frames if save_img==True
        write_parquet(qualities, pdq_hashes, frames_img, pdq_type, pdq_change, timestamps, wpath, name, ref, ID,
                    frame_class, region_type, save_img)

        # closing the video
        cv2.destroyAllWindows()
        cap.release()
        print("Time taken for video segmentation and fingerprinting is:", time.time() - video_start_time)
        return prev_hv
    except Exception as error:
        print(error)
        pass
    # return frames_img,pdq_hashes,qualities,pdq_type, timestamps,ID


# audio segmentation training
def train_hmm(data_path, win, step, fs, hmmModel):
    """
    arguments:-
        data_path: Path of Directory of the Data to train
        win: window to get features(441->10msec)
        step: overlap (step=441 means window to jump)if win=4410(100msec),step=2205(50msec) then features are calculated with 50% overlap
        fs:sampling rate
        hmmModel:name to save the trained Hmm model

    retuns:
        A message
          Suceess or failure
    function:-
        Trains a Hmm model for audio segmentation

    """
    try:
        start_time = time.time()
        # getting  all wav files in the folder
        files = []
        for i, (dirpath, dirnames, filenames) in enumerate(os.walk(data_path)):
            if (dirpath is not data_path):
                for f in filenames:
                    if ('.wav' in f):
                        files.append(os.path.join(dirpath, f))
        # deriving the necessary features and labels
        features = np.empty((68, 0))
        ind = 0
        labels = np.empty((1, 0))
        for file in files:
            signal, fs = librosa.load(file, sr=fs)
            feat, feat_names = STF.feature_extraction(signal, sampling_rate=fs,
                                                      window=win, step=step, deltas=True)
            # features - (#feats x #frames)
            features = np.hstack((features, feat))
            labels = np.hstack((labels, ind * np.ones((1, feat.shape[1]))))
            # labels = np.hstack((labels,ind*np.ones((1,features.shape[1]))))
            ind += 1
        # training hmm based model
        cl_priors, trans_mat, means, cov = AS.train_hmm_compute_statistics(features, labels.T)
        hmm = hmmlearn.hmm.GaussianHMM(cl_priors.shape[0], "diag")
        hmm.covars_ = cov
        hmm.means_ = means
        hmm.startprob_ = cl_priors
        hmm.transmat_ = trans_mat

        AS.save_hmm(hmmModel, hmm, np.unique(labels).tolist(), win, step)
        print('time_taken: ' + str(time.time() - start_time))
        return "Model trained"
    except Exception as er:
        print("error occured while training the model check error messgae", er)
        return er


'''
    analyse code will run infinitely and produces output after every one sec.
    TODO:
        1: Run the code after 10 sec(can be run immediately depends on second process which generate wav file from strem).
        2: Then load the file one by one and segment. 
'''


def analyse_audio(filename , name, wpath , is_start = False, saved_buffer = None, is_split=False, dt=2,
                  model="models/hmmSample4", ID=None, debug=False,
                  timestamp_start_time=0, window1=1000, window2=2000  , number_of_skips = 4 , prev_last_time = 0):
    
    try:
        ID = name.split('_')[0]
        start_time = time.time()
        # loading  the model from the given path
        with open(model, 'rb') as f:
            hmm = cp.load(f)
            classes = cp.load(f)

        if (".ts" in filename) or (".mp4" in filename):
            fname_ = '/var/admon/WAV_FILES'
            fname = fname_ + '/' + name + '.wav'
            check_folder = os.path.isdir(fname_)
            if not os.path.exists(fname_):
                os.makedirs(fname_)

            print("using wav file ", name)
            #change code to skip the next Popen if filename does not exist
            if os.path.exists(filename) == False:
                return 0, 0, 10
            process = Popen(('ffmpeg', '-y', '-i', filename, '-ac', '2', fname), stdout=PIPE, stderr=PIPE)
            stdout, stderr = process.communicate()
            if process.returncode != 0:
                x = str(stderr).split('\\n')
                if "Output file #0 does not contain any stream" in x[-2]:
                    print("There is no audio for this video files")
                    raise IOError("Video doesn't contain any audio")
                else:
                     #print("stderr", stderr)
                     return 0, 0, 10
                     #return stderr
        elif '.wav' in filename:
            fname = filename
        else:
            return "Upload Proper format file"

        #print("Wavefile" , fname)
        
        
        # signal, fs = librosa.load(fname, sr=44100)
        proc_start_time = time.time()
        signal, fs = librosa.load(fname, sr=None)
        print("librosa.load() time =", time.time()-proc_start_time)
        
      
        if is_start == False:
            signal = np.concatenate([saved_buffer , signal])

        features, features_names = STF.feature_extraction(signal, fs, (4*fs)//10, fs//10)
        print(f"filename {fname} , {fs}")

        fts = features.T
        labels = hmm.predict(fts)
        segs, classes = AS.labels_to_segments(labels, 4410)
        # labels for 2 window sizes using above features(considers window range features as enitre audio features and calclates labels)
        window1_labels = []
        window2_labels = []
        
        step_w1 = window1 // 100
        for start in range(0, int(fts.shape[0]), step_w1):
            labels_w1 = hmm.predict(fts[start:start + step_w1, :])
            window1_labels.extend(labels_w1.tolist())

        # labels for window 2
        step_w2 = window2 // 100
        for start in range(0, int(fts.shape[0]), step_w2):
            labels_w2 = hmm.predict(fts[start:start + step_w2, :])
            window2_labels.extend(labels_w2.tolist())

        # Generating Fingerprints
        fingerprints = []
        # for first features assign its sign as fingerprint
        # check first features sign
        for i in range(dt):
            fins = ""
            for k in range(13):
                if (fts[i][8:22][k] > 0):
                    fins = fins + "1"
                else:
                    fins = fins + "0"
            fbit = hex(int(fins, base=2))
            fbit = '0x' + fbit[2:].zfill(4)
            fingerprints.append(fbit)

        # for each feature subtract j-dt feature and check sign to assingn fingerprints
        for j in range(0, fts.shape[0] - dt):
            fins = ""
            # for each mfcc feature
            for k in range(13):
                if (fts[j + dt][8:22][k] - fts[j][8:22][k] > 0):
                    fins = fins + "1"
                else:
                    fins = fins + "0"
            fbit = hex(int(fins, base=2))
            fbit = '0x' + fbit[2:].zfill(4)
            fingerprints.append(fbit)

        # a Dictionary to store all the values
        final_dict = {}
        
        ffprobe_time = time.time()
        #getting number of channels using ffprobe(ffmpeg)
        command_array = ["ffprobe" , "-v" , "quiet" ,  "-print_format" ,  "json" ,  "-show_format" ,  "-show_streams" ,  "-select_streams" ,  "a:0" , fname]
        result = subprocess.run(command_array, shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
        if result.returncode == 0:
            result = six.ensure_str(result.stdout , 'ascii')
            result_json = json.loads(result)
            final_dict['channels'] = int(result_json['streams'][0]["channels"])
            duration = float(result_json['streams'][0]["duration"])
        else:
            print("Not working")
            return result.stderr
        print('ffprobe : ' + str(time.time() - ffprobe_time))

        final_dict['features_names'] = features_names
        final_dict['labels'] = labels
        final_dict['window1_labels'] = window1_labels
        final_dict['window2_labels'] = window2_labels
        final_dict['segments'] = segs

        # convert silence range to 0-100
        silence_score = []
        volume = []
        for energy in features[1, :]:
            volume.append(int(10.0 * (math.log(energy, 10) - 3.343408593803857)))
            if energy < 0.00221:
                silence_score.append(1)
            else:
                silence_score.append(0)
        
        final_dict['silence'] = silence_score
        final_dict['volume'] = volume
        final_dict['features'] = fts
        final_dict['fingerprints'] = fingerprints
        
        # adding timestamps checking if its a split so that it can continue from split time
        if is_split:
            # total_time=features.shape[1]*((1/441)*mfcc_step*10)
            timestamps = []
            for i in range(int(timestamp_start_time), int((features.shape[1] * 100) + timestamp_start_time), 100):
                # start=400 beacuse window=400msec and step=100  above because of the step used for features calculation
                timestamps.append(400 + i)
            
            timestamp_saved = 400+i

            for i in range(0, int(features.shape[1] * (100)), 100):
                pass
            last_time = 400+i

        else:
            timestamps = []
            for i in range(0, int(features.shape[1] * (100)), 100):
                timestamps.append(400 + i)


        #saved_duration = int(signal.shape[0] - (last_time - prev_last_time)*44.100) + int(400*44.1)
        saved_duration = int(signal.shape[0] - last_time*44.100) + int(number_of_skips*4410)
        # print('*'*70)
        # print(saved_duration , signal.shape , last_time*44.1  , last_time , prev_last_time , last_time-prev_last_time)
        
        saved_buffer = signal[-saved_duration:]
    
        final_dict['timestamps'] = timestamps
        # storing the results into tsv and praquet file
        write_audio_parquet(final_dict, wpath, name, ID, debug)
        # pr=pd.read_parquet(pz_file)

        file_delete_operation = time.time()
        while os.path.exists(fname):
            os.remove(fname)
        print('time taken to delete the file : ' + str(time.time() - file_delete_operation))

        print('time_taken for audio segmentation and fingerprinting is : ' + str(time.time() - start_time))

        return saved_buffer , timestamp_saved , duration
    
    except Exception as error:
        print(error)
        pass
