README for fpt_audio

version 0.17

This code wraps a Python audio-fingerprinting module in C++, and runs
audio samples from a file or stream through the fingerprinting method.
The fingerprinting method writes fingerprints to a file.

BUILD

The current Makefile is set up for Python 3.6.9.  To use it with another
version of Python, you must change CXXFLAGS and LDFLAGS in the Makefile. 
For recent versions, you might just change the 2- and 3-digit Python version
numbers in these strings.  However, here is the correct method.

1) Find the installed version of Python that you wish to use.  Note its
2-digit version number M.N.

2) To get the option string for CXXFLAGS, type

pythonM.N-config --cflags

In the Makefile, replace the entire option string for CXXFLAGS by the string
returned by this command.  You should add the -fPIC option.

3) To get the option string for LDLAGS, type

pythonM.N-config --ldflags

In the Makefile, replace the entire option string for LDFLAGS by the string 
returned by this command.

In (2) and (3), you might find that some options, such as -g, are repeated.

4) Type

make (or make -f Makefile)

The code should compile.

C++ REQUIREMENTS

The C++ code uses the C APIs for Python and NumPy, which should be installed
with their respective parent packages.  Also, it uses the FFmpeg command-line
tools, which should be installed as part of the Python reuiremnts.

PYTHON REQUIREMENTS

The Python fingerprinting code has the same pre-requisites as the "pure"
Python code from which it is derived.  One exception is librosa, which is
not explicity called here.  Also, the audio fingerprinting code does not
use OpenCV.

USAGE

fpt_audio <input_file_stream_name> <fpt_file_root_name> <max_num_seconds>

FILES IN THIS REPOSITORY

README.md	this file
Makefile	makefile; currently the same as the 3.6 version below
Makefile_Py2_7	makefile for Python 2.7.17
Makefile_Py3_6	makefile for Python 3.6.9
main.py		the Python code
libraries.py	to inport prerequisite Pythano modules
utils.py	utilities, such as writing otput files
pyhelper.hpp	C++ classes that make the setup and cleanup of the C-API
		objects a lot easier.  Taken from a CodeProject tutorial.
fpt_audio.cpp	the "main" C++ code.  The main function, plus functions
		to set up a pipe from FFmpeg, and interfacing to Python
		and NumPy.

ISSUES

The pure Python code is known to work.  This code is still being tested,
hence the versions.  It should compile and give a usage line if no
command-line parameters are given.

v0.1	initial version, compiles; gives usage line
v0.11	working makefiles for C APIs for Python 2.7 and 3.6
v0.12	correct local paths; added model dir and file; correct string
	format to __CallFunction method
v0.13	corrected FFmpeg command string;  import of Python module fails
	fail, but previous setup works, as does the audio input
v0.14	major update to bring in line with "pure" Python code and add
	more needed parameters to the main method call; Python module
	import in Python3 interpreter but not here, although C-API usage
	appears correct
v0.15	Import problem fixed; seg fault setting up NumPy array fixed;
	debug output enabled with ifdefs; segfault with Python function
	call to be fixed
v0.16 	Fixed segfault; generates fingerprints; minor problems remain
v0.17	added 44.1k resampling of input audio

BIG ISSUES AND ARCHITECTURE

Once debugged, the code will read in 5 seconds of audio at a time to fill
the audio buffer, fingerprint this, and write out a file for the features,
fingerprints, and metadata for this segment.

The segment length will be made a parameter.  Actually, we should have
a config file (JSON?) to set all useful parameters.

Currently, the Python method fingerprints each segment as a separate
unit.  This means that audio-processing frames do not cross 5-second
boundaries, so the output will miss features near segment boundaries.

The parent Python repo has an option to split input files, which might
help here.

We can implement overlapping segments very easily by reading in smaller
segments and assembling them into a larger buffer or a queue of samples.
For example, we could read in 5 second segments, but fingerprint a 15-
or 20-second buffer of samples every 5 seconds.  In a naive implementation,
this requires no changes to the core Python fingerprinting method.

This would avoid some of the problem of missing features at 5-second
boundaries.

However, this approach is inefficient in computation, memory, and output
files.  It is much better to enable fingerprinting across the segment
boundaries and collect features into overlapping larger segments in the
matching and search.

We can, in fact, do this by

a) constructing a buffer of slightly more than 5 seconds every 5 seconds
b) only fingerprinting the innermost 5 seconds of said buffer.

Essentially, this is a problem of indexing samples and processing windows.
The extra cost in (a) should be small compared to the total processing cost
in (b).

Once the basic fingerprinting actually works, this should be a relatively
simple change.
