from  libraries import *


#some variables assignment
sliding_cnt = 0
min_diff = 1000
max_diff = 0
min_fr = 0
max_fr = 0
sliding_win = 30
max_qlen = sliding_win
DEBUG = 0
local_path= "pdq"
f_path = "frames"

# function to write reference files ,frame_time starts with 0, #for broadcast stream its different
def write_parquet(qualities, pdq_hashes, frames, pdq_type, pdq_change, timestamps, wpath, name, ref, ID, frame_class,
                  region_type, save_img=0):
    file_out = wpath + "/" + local_path + "/" + name + "/" + name + "_" + time.strftime('%Y%m%d%H%M%S') + ".tsv"
    parquet_file = wpath + "/" + local_path + "/" + name + "/" + name + "_" + time.strftime('%Y%m%d%H%M%S') + ".parquet"

    cnt = 0

    check_folder = os.path.isdir(wpath + "/" + local_path + "/" + name)
    if not check_folder:
        os.makedirs(wpath + "/" + local_path + "/" + name)

    check_folder = os.path.isdir(wpath + "/" + f_path + "/" + name)
    if not check_folder:
        os.makedirs(wpath + "/" + f_path + "/" + name)

    with open(file_out, 'wt') as out_file:
        try:
            tsv_writer = csv.writer(out_file, delimiter='\t')
            ##adding extra columns region_class,region_type
            header = ["fp", "type", "lastdiff", "quality", "time", "id", "region_class", "region_type"]
            tsv_writer.writerow(header)

            while (cnt < len(qualities)):
                try:
                    hv = hex(int(str(pdq_hashes[cnt]).strip("array([])").replace(" ", "").replace("\n", ""), 2))

                    tsv_writer.writerow([hv, pdq_type[cnt], pdq_change[cnt], \
                                         str(qualities[cnt]), timestamps[cnt], ID, frame_class[cnt], region_type[cnt]])

                    # writing frames
                    # if save ==1 saves images to the folder wpath/frames
                    if save_img == 1:
                        if ref == 1:
                            frame_name = "frame%d" % cnt + ".jpg"
                            frame_path = os.path.join(wpath + "/" + f_path + "/" + name + "/",
                                                      "frame_%s" % name + "_" + str(timestamps[cnt]) + ".jpg")
                        else:
                            frame_name = "frame%d" % cnt + "_" + datetime.now().strftime(
                                "%Y_%m_%d-%I_%M_%S_%p") + ".jpg"
                            frame_path = os.path.join(wpath + "/" + f_path + "/" + name + "/",
                                                      "frame_%s" % name + "_" + str(timestamps[cnt]) + ".jpg")
                        cv2.imwrite(frame_path, frames[cnt])
                    cnt += 1
                except:
                    print("cnt, pdq_hashes[cnt], hv", cnt, pdq_hashes[cnt], hv, pdq_type[cnt], pdq_change[cnt], \
                          qualities[cnt], timestamps[cnt])
                    traceback.print_exc()
                    break
        except:
            traceback.print_exc()
            # write data to tsv and parquet file
    df = pd.read_csv(file_out, delimiter='\t')

    df.to_parquet(parquet_file)


# 1. we want to implement a FIFO structure to keep latest frames of size sliding_window
#  # It turns out that with ring buffer you have direct access to any image
# 2. From these given frames we want to detect min_frames where PDQ diff is min, and similarly max
# 3. since we are skipping frames, we want to check again in the gaps on both sides of min and max to see if
# the actual min or max frame is between the gaps
# 4. For these selected frames -> we will generate PDQ and save it to our json
# 5. the example below is in block; really we. want a moving sliding window, for which we will need to change logic
# below

# Numpy Ring Buffer
def save_frame_ring(buff, picture, max_qlen):
    if len(buff) >= max_qlen:  # if actual length of q is bigger than win
        buff.popleft()
    # clone_img = copy.copy(original_img)
    buff.append(picture)


def hamming_distance_set(s1, s2):
    """Return the Hamming distance between equal-length sequences."""
    if len(s1) != len(s2):
        raise ValueError("Undefined for sequences of unequal length.")
    sz1 = 16
    sz2 = 64
    diff1 = sum(el1 != el2 for el1, el2 in zip(s1[:sz1], s2[:sz1]))
    diff2 = sum(el1 != el2 for el1, el2 in zip(s1[:sz2], s2[:sz2]))
    return diff1, diff2, sum(el1 != el2 for el1, el2 in zip(s1, s2))


# import traceback
# DEBUG = None
# function to calculate last dif and select time stamps and generate fingerprints
def calcs(buff_temp, buff, cap, ID, start_fr, sliding_win, frames, pdq_hashes, qualities, pdq_type, pdq_change,
          crop_region, frame_class, region_type, prev_hv=None, frames_to_jump=3):
    timestamps = []

    frames_examined = 0
    frames_counter = start_fr

    min_diff = 256
    max_diff = 0
    min_diff_low = 256
    max_diff_low = 0
    max_fr = 0
    min_fr = 0
    max_fr_low = 0
    min_fr_low = 0
    max_hash = 0
    min_hash = 0
    max_hash_low = 0
    min_hash_low = 0
    max_quality = 0
    min_quality = 200
    max_quality_low = 0
    min_quality_low = 200

    en_min = 0
    en_max = 0

    while (buff):
        frame = buff.popleft()
        try:
            if DEBUG == 1 and frames_counter > 1760:
                print ("frames_counter", frames_counter)
            ## skipping frames and processing
            if (frames_counter % frames_to_jump == 0) and frames_counter > 0:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                ## computing  quality and hash vector
                hash_vector, quality = pdqhash.compute(cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB))

                # calculating pdq_difference and min and max frames
                if quality == 100:
                    # pdq_diff = hamming_distance(hash_vector, prev_hv)
                    pdq_diff1, pdq_diff2, pdq_diff3 = hamming_distance_set(hash_vector, prev_hv)
                    pdq_diff = int((pdq_diff1 * 32 + pdq_diff2 * 16 + pdq_diff3) / 4)
                    if DEBUG == 1:
                        print ("frame, pd_diff, quality", frames_counter, pdq_diff, quality)
                    if pdq_diff < min_diff:
                        min_fr = frames_counter
                        min_diff = pdq_diff
                        min_hash = hash_vector
                        min_quality = quality
                    if pdq_diff > max_diff:
                        max_fr = frames_counter
                        max_diff = pdq_diff
                        max_hash = hash_vector
                        max_quality = quality
                else:
                    # pdq_diff = hamming_distance(hash_vector, prev_hv)
                    pdq_diff1, pdq_diff2, pdq_diff3 = hamming_distance_set(hash_vector, prev_hv)
                    pdq_diff = int(pdq_diff1 * 32 + pdq_diff2 * 16 + pdq_diff3) / 4
                    if DEBUG == 1:
                        print ("frame, pd_diff, quality", frames_counter, pdq_diff, quality)
                    if pdq_diff < min_diff_low:
                        min_fr_low = frames_counter
                        min_diff_low = pdq_diff
                        min_hash_low = hash_vector
                        min_quality_low = quality
                    if pdq_diff >= max_diff_low:
                        max_fr_low = frames_counter
                        max_diff_low = pdq_diff
                        max_hash_low = hash_vector
                        max_quality_low = quality

                prev_hv = hash_vector
                if DEBUG:
                    print ("0. min_fr, max_fr, min_fr_low,  max_fr_low, pdq_diff1, pdq_diff2, pdq_diff, quality", \
                           min_fr, max_fr, min_fr_low, max_fr_low, pdq_diff1, pdq_diff2, pdq_diff, quality)
                    # print ("frame ounter", frames_counter)
        except:
            # traceback.print_exc()
            pass

        if frames_counter == 0:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            hash_vector, quality = pdqhash.compute(cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB))
            prev_hv = hash_vector
        frames_examined = frames_examined + 1
        frames_counter += 1

    # end of while loop
    if (frames_counter % sliding_win == 0) or len(buff) != sliding_win:
        # check if min_fr or max_fr is within the new sliding window  or is the last batch ie can be less than sliding_win
        if DEBUG == 1:
            print("frames_counter", frames_counter)
        if max_fr < frames_counter - sliding_win and max_fr_low >= frames_counter - sliding_win:
            max_fr = max_fr_low
            max_diff = max_diff_low
            max_hash = max_hash_low
            max_quality = max_quality_low
        if max_fr >= frames_counter - sliding_win:
            en_max = 1

        # append lowest frame first

        if min_fr < frames_counter - sliding_win and min_fr_low >= frames_counter - sliding_win:
            min_fr = min_fr_low
            min_diff - min_diff_low
            min_hash = min_hash_low
            min_quality = min_quality_low
        if min_fr >= frames_counter - sliding_win:
            en_min = 1

        # first frame
        # print("frame 1")
        if DEBUG == 1:
            print ("en_max, en_min", en_max, en_min)
        if en_max and (max_fr < min_fr or en_min == 0):
            # print("outside max frame")
            frames.append(max_fr)
            pdq_hashes.append(max_hash)
            qualities.append(max_quality)
            pdq_type.append(1)
            pdq_change.append(max_diff)
            region_class = 0  # add result of frame region classification
            frame_class.append(region_class)
            region_type.append(0)
            # append for second region
            if len(crop_region) > 0:
                # for i in range(len(crop_region)):
                frame_temp = buff_temp[max_fr - start_fr]
                frame2 = frame_temp[crop_region[0]:crop_region[0] + crop_region[2],
                         crop_region[1]:crop_region[1] + crop_region[3]]
                # frame2 = frame2[crop_region[i][0]:crop_region[i][0] + crop_region[i][2],crop_region[i][1]:crop_region[i][1] + crop_region[i][3] ]
                frame2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
                hash_vector2, quality2 = pdqhash.compute(cv2.cvtColor(frame2, cv2.COLOR_GRAY2RGB))
                frames.append(max_fr)
                pdq_hashes.append(hash_vector2)
                qualities.append(max_quality)
                pdq_type.append(1)
                pdq_change.append(max_diff)
                region_class = 0  # add result of frame region classification
                frame_class.append(region_class)
                region_type.append(1)

            if DEBUG == 1:
                print ("1A.insert max_fr, pdiff, q", max_fr, max_diff, max_quality)
        elif en_min and ((max_fr > min_fr or en_max == 0) or en_min == 0 and en_max == 0):
            # write if min_fr (first) or only min_fr or no change at all (same frames)
            frames.append(min_fr)
            # print("outside min frame")
            pdq_hashes.append(min_hash)
            qualities.append(min_quality)
            pdq_type.append(0)
            pdq_change.append(min_diff)
            region_class = 0
            frame_class.append(region_class)
            region_type.append(0)
            # append for second region
            if len(crop_region) > 0:
                frame_temp = buff_temp[min_fr - start_fr]
                frame2 = frame_temp[crop_region[0]:crop_region[0] + crop_region[2],
                         crop_region[1]:crop_region[1] + crop_region[3]]
                frame2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
                hash_vector2, quality2 = pdqhash.compute(cv2.cvtColor(frame2, cv2.COLOR_GRAY2RGB))
                frames.append(min_fr)
                pdq_hashes.append(hash_vector2)
                qualities.append(min_quality)
                pdq_type.append(1)
                pdq_change.append(min_diff)
                region_class = 0  # add result of frame region classification
                frame_class.append(region_class)
                region_type.append(1)

            if DEBUG == 1:
                print ("1B. insert min_fr, pdiff, q", min_fr, min_diff, min_quality)
        # second frame write
        # print("frame 2")
        if en_min and en_max and max_fr > min_fr:
            # print("outside max frame")
            frames.append(max_fr)
            pdq_hashes.append(max_hash)
            qualities.append(max_quality)
            pdq_type.append(1)
            pdq_change.append(max_diff)
            region_class = 0
            frame_class.append(region_class)
            region_type.append(0)
            # append for second region
            if len(crop_region) > 1:
                frame_temp = buff_temp[max_fr - start_fr]
                frame2 = frame_temp[crop_region[0]:crop_region[0] + crop_region[2],
                         crop_region[1]:crop_region[1] + crop_region[3]]
                frame2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
                hash_vector2, quality2 = pdqhash.compute(cv2.cvtColor(frame2, cv2.COLOR_GRAY2RGB))
                frames.append(max_fr)
                pdq_hashes.append(hash_vector2)
                qualities.append(max_quality)
                pdq_type.append(1)
                pdq_change.append(max_diff)
                region_class = 0  # add result of frame region classification
                frame_class.append(region_class)
                region_type.append(1)

            if DEBUG == 1:
                print ("2A.insert max_fr, pdiff, q", max_fr, max_diff, max_quality)
        elif en_min and en_max and max_fr < min_fr:
            # print("outside max frame")
            frames.append(min_fr)
            pdq_hashes.append(min_hash)
            qualities.append(min_quality)
            pdq_type.append(0)
            pdq_change.append(min_diff)
            region_class = 0
            frame_class.append(region_class)
            region_type.append(0)
            # append for second region
            if len(crop_region) > 0:
                frame_temp = buff_temp[min_fr - start_fr]
                frame2 = frame_temp[crop_region[0]:crop_region[0] + crop_region[2],
                         crop_region[1]:crop_region[1] + crop_region[3]]
                frame2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
                hash_vector2, quality2 = pdqhash.compute(cv2.cvtColor(frame2, cv2.COLOR_GRAY2RGB))
                frames.append(min_fr)
                pdq_hashes.append(hash_vector2)
                qualities.append(min_quality)
                pdq_type.append(1)
                pdq_change.append(min_diff)
                region_class = 0  # add result of frame region classification
                frame_class.append(region_class)
                region_type.append(1)
            if DEBUG == 1:
                print ("2B.insert min_fr, pdiff, q", min_fr, min_diff, min_quality)

                # print ("min_fr, max_fr", min_fr, max_fr)
    # print("0. len of qualities,pdq_hashes,frames,pdq_type, pdq_diff ", len(qualities),len(pdq_hashes),len(frames),len(pdq_type), len(pdq_change))
    return prev_hv



#Storing data to output files (tsv and parquet)
def write_audio_parquet(final_dictionary,wpath,name,Id,debug=False):
  """
  arguments:-
      final_dictionary: Dictionary with all the required values(id,fingerprints,timestamps,features)
      wpath: Directory to write the output files (parqurt,tsv file)
      name: Name of the file (used to for naming output files)
      Id: A unique number given to Identify files
      debug: Boolean
              True: Writes required and all Features  to tsv and paqquet
              False: Writes only required Features to tsv and paqquet
  Return:-
      None
  function:-
      Creates two files tsv and parquet
  """
  local_path="pdq"
  f_path="frames"
  file_out = wpath + "/" +local_path + "/" + name + "/" + name +"_"+ time.strftime('%Y%m%d%H%M%S')+"_audio.tsv"
  parquet_file = wpath + "/" +local_path + "/"+ name + "/" + name +"_"+ time.strftime('%Y%m%d%H%M%S')+ "_audio.parquet"
  cnt = 0
  check_folder = os.path.isdir(wpath+ "/" + local_path +"/"+ name )
  if not check_folder:
    os.makedirs(wpath+ "/" +local_path+"/"+name)

  #writing into tsv file row by row
  with open(file_out, 'wt') as out_file:
    try:
      tsv_writer = csv.writer(out_file, delimiter='\t')
      if debug:
        header = ["id","channel", "labels", "segments", "volume", "silence","timestamps","fingerprints"]
        #adding features names
        header.extend(final_dictionary['features_names'])
        tsv_writer.writerow(header)
        ftrs=final_dictionary['features']
        segs=final_dictionary['segments'].tolist()
        while(cnt<len(final_dictionary['fingerprints'])):
          try:
            try:
              row_result=[Id,final_dictionary['channels'],final_dictionary['labels'][cnt],segs[cnt],final_dictionary['volume'][cnt],final_dictionary['silence'][cnt],final_dictionary['timestamps'][cnt],final_dictionary['fingerprints'][cnt]]
              row_features=[ftrs[cnt][i] for i in range(68)]
              row_result.extend(row_features)
              tsv_writer.writerow(row_result)
            except IndexError as e:
              row_result=[Id,final_dictionary['channels'],final_dictionary['labels'][cnt],0,final_dictionary['volume'][cnt],final_dictionary['silence'][cnt],final_dictionary['timestamps'][cnt],final_dictionary['fingerprints'][cnt]]
              row_features=[ftrs[cnt][i] for i in range(68)]
              row_result.extend(row_features)
              tsv_writer.writerow(row_result)
            cnt=cnt+1
          except:
            traceback.print_exc()
            break
      else:
        header = ["id","channels", "labels", "volume", "silence","timestamps","fingerprints","window1_labels","window2_labels"]
        tsv_writer.writerow(header)
        ftrs=final_dictionary['features']
        while(cnt<len(final_dictionary['fingerprints'])):
          try:
            tsv_writer.writerow([Id,final_dictionary['channels'],final_dictionary['labels'][cnt],final_dictionary['volume'][cnt],final_dictionary['silence'][cnt],final_dictionary['timestamps'][cnt],final_dictionary['fingerprints'][cnt],final_dictionary['window1_labels'][cnt],final_dictionary['window2_labels'][cnt]])
            cnt=cnt+1
          except:
            traceback.print_exc()
            break
    except:
      traceback.print_exc()
  #writing output to a parqet file
  df = pd.read_csv(file_out, delimiter='\t')
  df.to_parquet(parquet_file)
  #return "Done"


# function for splitting large videos(uses ffmpeg)
def split_videos(file_path, files_out, split_length=60):
    '''
    arguments:
      filename:- path of the file to split
      split_length:- number of seconds to split
      files_out:- output path for storing the files(parts)

    returns:-
      List of filepaths after splitting
    '''
    # to match duration from ffmpeg output
    length_regexp = 'Duration: (\d{2}):(\d{2}):(\d{2})\.\d+,'
    re_length = re.compile(length_regexp)

    check_folder = os.path.isdir(files_out)
    if not check_folder:
        os.makedirs(files_out)
    if files_out[-1] == "/":
        files_out = files_out[:-1]
    if split_length <= 0:
        print("Split length can't be 0")
        raise SystemExit

    p1 = Popen(["ffmpeg", "-y", "-i", file_path], stdout=PIPE, stderr=PIPE, universal_newlines=True)
    # get p1.stderr as input
    output = Popen(["grep", 'Duration'], stdin=p1.stderr, stdout=PIPE, universal_newlines=True)
    p1.stdout.close()
    matches = re_length.search(output.stdout.read())
    if matches:
        video_length = int(matches.group(1)) * 3600 + \
                       int(matches.group(2)) * 60 + \
                       int(matches.group(3))
        print("Video length is seconds: {}".format(video_length))
    else:
        return "Can't determine video length."

    split_count = math.ceil(video_length / split_length)
    split_list = []
    if split_count == 1:
        split_list.append(file_path)
        return split_list

    for n in range(split_count):
        split_start = split_length * n
        pth, ext = file_path.rsplit(".", 1)
        head, filename = ntpath.split(file_path)
        cmd = "ffmpeg -y -i {}  -vcodec copy  -strict -2 -ss {} -t {} {}-{}.{}". \
            format(str(file_path), split_start, split_length, files_out + "/" + filename[:-4], n, ext)
        split_list.append(files_out + "/" + filename[:-4] + "-" + str(n) + "." + ext)
        # print("About to run: {}".format(cmd))
        check_call(shlex.split(cmd), universal_newlines=True)
    return split_list


# use this to run on all ads
# function below runs audio and video fingerprinting for multiple files passed in the list
def analyse_audio_video(files_list, np, wpath, dt):
    '''
    arguments:
      files_list: list if files to compute fingerprints
      np: number of parallel process to run
      wpath: output path for storing the files(tsv,parquet and frames)
      dt=t-i th feature used to compute fingerprint (used for audio segmentation)
      Note: Use this only for small videos(max 1hr)
    returns:-
      None
    function:-
      Generates the audio and video fingerprints
    '''

    # imports
    import main
    import concurrent.futures
    from concurrent.futures import ProcessPoolExecutor
    # from functools import partial

    num_of_parallel_processes
    mp_start_time = time.time()
    with concurrent.futures.ProcessPoolExecutor(max_workers=np) as executor:
        # you might want to shift the args that change for every file to the front of the args in the actual functions

        for mp4_file in files_list:
            audio_futures = {executor.submit(main.analyse_audio, mp4_file, mp4_file.split('/')[-1][:-4], wpath, dt): (
            main.analyse_audio, mp4_file, mp4_file.split('/')[-1][:-4], wpath, dt)}
            video_futures = {executor.submit(main.analyse_video, mp4_file, mp4_file.split('/')[-1][:-4], wpath): (
            main.analyse_video, mp4_file, mp4_file.split('/')[-1][:-4], wpath)}
        for future in concurrent.futures.as_completed(audio_futures):
            print("result={}".format(future.result()))
        for future in concurrent.futures.as_completed(video_futures):
            print("result={}".format(future.result()))
    print(f'Total time to run multithreads: {time.time() - mp_start_time:2f}s')
