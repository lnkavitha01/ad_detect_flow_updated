README for fpt_video

version 0.11

This code wraps a Python video-fingerprinting module in C++, and runs
video frames from a file or stream through the fingerprinting method.
The fingerprinting method writes fingerprints to a file.

The current code uses the original Python code as much as possible.
The Python method uses OpenCV to read the frames.  The C++ code passes
filenames and parameters to the method.

BUILD

The current Makefile is set up for Python 3.6.9.  To use it with another
version of Python, you must change CXXFLAGS and LDFLAGS in the Makefile. 
For recent versions, you might just change the 2- and 3-digit Python version
numbers in these strings.  However, here is the correct method.

1) Find the installed version of Python that you wish to use.  Note its
2-digit version number M.N.

2) To get the option string for CXXFLAGS, type

pythonM.N-config --cflags

In the Makefile, replace the entire option string for CXXFLAGS by the string
returned by this command.  You should add the -fPIC option.

3) To get the option string for LDLAGS, type

pythonM.N-config --ldflags

In the Makefile, replace the entire option string for LDFLAGS by the string 
returned by this command.

In (2) and (3), you might find that some options, such as -g, are repeated.

4) Type

make (or make -f Makefile)

The code should compile.

C++ REQUIREMENTS

The C++ code uses the C APIs for Python and NumPy, which should be installed
with their respective parent packages.

PYTHON REQUIREMENTS

The Python fingerprinting code has the same pre-requisites as the "pure"
Python code from which it is derived.

USAGE

fpt_video <input_file_stream> <fpt_filename>
          <region_filename> <segment_time_sec> <total_time_sec>

input_file_name	name of the input stream or file
fpt_filename root name of the output files
region_filename the file with the parameters for the frame regions
to be processed
segment_time time in seconds for one segment
total_time_seconds total duration of video to be processed	

The region file should have one or two rows, each with 4 
parameters, separated by spaces.  In one row, we have

<top_row> <left_col> <width> <height>

with the row and col indices starting from 0.

FILES IN THIS REPOSITORY

README.md	this file
Makefile	makefile; currently the same as the 3.6 version below
Makefile_Py2_7	makefile for Python 2.7.17
Makefile_Py3_6	makefile for Python 3.6.9
main.py		the Python code
libraries.py	to import prerequisite Python modules
utils.py	utilities, such as writing output files
pyhelper.hpp	C++ classes that make the setup and cleanup of the C-API
		objects a lot easier.  Taken from a CodeProject tutorial.
fpt_video.cpp	the "main" C++ code.

ISSUES

The pure Python code is known to work.  This code is still being tested,
hence the versions.  It should compile and give a usage line if no
command-line parameters are given.

v0.1	initial version, compiles; gives usage line
v0.11	fixed import bug

THis is based on fpt_audio v0.17

BIG ISSUES AND ARCHITECTURE

<LATER>
