// Audio fingerprinting main()
 
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <Python.h>
#include <numpy/npy_common.h>
#include <numpy/arrayobject.h>
#include "pyhelper.hpp"

#define __FPT_DEBUG
#define __FPT_ENABLE

using namespace std;

int fingerprint_video(string input_file_stream,
                      string fpt_filename,
		      int duration, // Double in future?
		      int num_regions,
		      int regions[2][4],
		      CPyObject& pModule)
{
#ifdef __FPT_DEBUG
  cerr << "Entered fingerprint_video" << endl;
#endif

#if 0
  CPyInstance hInstance;

  import_array()

  // Fingerprinting module
    
  CPyObject pName = PyUnicode_FromString("main");
  CPyObject pModule = PyImport_Import(pName);

  if (!pName)
  {
    cerr << "Error: can't create Python string" << endl;
    return(-1);
  }

  if (!pModule)
  {
    cerr << "Error: fingerprint module not found" << endl;
    return(-1);
  }

#ifdef __FPT_DEBUG
  cerr << "Imported module" << endl;
#endif

#else

  import_array()

#endif
    
  // Set up the function
    
  CPyObject pFunc = PyObject_GetAttrString(pModule, "analyse_video");
  
  if (!pFunc || !PyCallable_Check(pFunc))
  {
    cerr << "Error: getting method analyse_video()" << endl;
    return(-1);
  }

#ifdef __FPT_DEBUG
  cerr << "Got method" << endl;
#endif

  // Set up the NumPy regions array

  long int dims[2];

  dims[0] = num_regions;
  dims[1] = 4;

  #ifdef __FPT_DEBUG
  cerr << "Just before PyArray_SimpleNewFromData" << endl;
#endif

   CPyObject pRegions = (CPyObject) PyArray_SimpleNewFromData(2,
                                                &dims[0],
                                                NPY_INT32,
                                                (void *) regions);

#ifdef __FPT_DEBUG
  cerr << "Just after PyArray_SimpleNewFromData" << endl;
#endif

  if (!pRegions)
  {
    cerr << "Error: region array not created" << endl;
    return(-1);
  }

#ifdef __FPT_DEBUG
  cerr << "Set up NumPy array" << endl;
#endif
    
  // Call the function
  // The odd-looking string is a format string for the inputs
  //
  // O = object
  // i = integer
  // s = string

#ifdef __FPT_DEBUG
  cerr << "Just before PyObject_CallFunction" << endl;
#endif
    
  CPyObject pValue = PyObject_CallFunction(pFunc, "sssiO",
                                           input_file_stream.c_str(),
                                           fpt_filename.c_str(),
					   "./fingerprints",
					   duration,
					   pRegions);

#ifdef __FPT_DEBUG
  cerr << "Just after PyObject_CallFunction" << endl;
#endif

  // We don't have a consistent success/fail return code yet
  // We should add exception handling

  return 0;
}

int main(int argc, char **argv)
{
  string input_file_stream;
  string fpt_filename;
  string region_filename;
  double total_time;
  double elapsed_time;
  double segment_time;
  int regions[2][4];
  string region_line;
  int num_regions;
  int ret_code;

#if 0
  // Python setup

  CPyInstance hInstance;

  // Fingerprinting module
    
  CPyObject pName = PyUnicode_FromString("main");

  if (!pName)
  {
    cerr << "Error: can't create Python string" << endl;
    return(-1);
  }

  CPyObject pModule = PyImport_Import(pName);

  if (!pModule)
  {
    cerr << "Error: fingerprint module not found" << endl;
    return(-1);
  }

#ifdef __FPT_DEBUG
  cerr << "Imported module" << endl;
#endif

#endif
    
  // Command line

  if (argc < 6)
  {
      cerr << "usage: " << argv[0]
           << " input_file_stream fpt_filename region_filename segment_time_sec total_time_sec" << endl;
      
    return(-1);
  }

  input_file_stream = argv[1];
  fpt_filename      = argv[2];
  region_filename   = argv[3];
  segment_time      = atof(argv[4]);
  total_time        = atof(argv[5]);

#if 1
  // Python setup

  CPyInstance hInstance;

  // Fingerprinting module
    
  CPyObject pName = PyUnicode_FromString("main");

  if (!pName)
  {
    cerr << "Error: can't create Python string" << endl;
    return(-1);
  }

  CPyObject pModule = PyImport_Import(pName);

  if (!pModule)
  {
    cerr << "Error: fingerprint module not found" << endl;
    return(-1);
  }

#ifdef __FPT_DEBUG
  cerr << "Imported module" << endl;
#endif

#endif

  // Read in the region parameters

  ifstream region_file(region_filename);

  if (!region_file.is_open())
  {
    cerr << "Error: can't open regions file" << endl;
    return -1;
  }

  num_regions = 0;

  while (getline(region_file, region_line))
  {
    if (num_regions == 2)
      break;

    sscanf(region_line.c_str(), "%d %d %d %d\n",
           &regions[num_regions][0],
           &regions[num_regions][1],
           &regions[num_regions][2],
           &regions[num_regions][3]);

    num_regions++;
  }

  if (num_regions == 0)
  {
    cerr << "Error: no regions in region file" << endl; 
    return -1;
  }


  region_file.close();

  // Init for the main loop

  elapsed_time = 0;

  // The main loop

  while (elapsed_time < total_time)
  {
    // Fingerprint the video segment
      
#ifdef __FPT_ENABLE
    ret_code = fingerprint_video(input_file_stream,
                                 fpt_filename,
				 segment_time,
				 num_regions,
				 regions,
				 pModule);

#ifdef __FPT_DEBUG
    cout << "fingerprint_video returned " << ret_code << endl;
#endif

    if (ret_code < 0)
    {
      cerr << "Error: fingerprint_video failed" << endl;
      break;
    }
#endif

    // Increment the elapsed time

    elapsed_time += segment_time;
  }
}
