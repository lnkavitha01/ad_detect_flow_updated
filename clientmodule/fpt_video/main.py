# main.py
from libraries import *
from utils import *
# function to genarate video fingerprints
def analyse_video(file, name, wpath, total_duration, regions=[], max_qlength=30, ref=1, ID=None, save_img=0, DEBUG=0):
    """
    arguments:-
        file: Path of the file to process
        name: Name of the file (used to for naming output files)
        wpath: Output Directory to write the frames,parquet and tsv files
        regions: 2d array with region cordiantes to calculate fingerprints
        total_duration: the duration you want processed, in seconds
        max_qlength: max window length for ring buffer(Doubt)
        Id: A unique number given to Identify files
        save_img: Boolean
                    True(1): saves the frames to folder(wpath)
                    False(0):Doesn't save the frames
        ref: Boolean
                  Note:used only if save_img=True
                    True(1): saves the frames with timestamp in the name of frame
                    False(0): saves the image with name by adding timestamp and current time
        DEBUG: Boolean
                  True: Prints data and saves files
                  False: Just saves the files
    returns:
        None
    function:-
      Generates the Video fingerprints by using the "Calcs" function, also calls "write_parquet"  to save the output to files
    """
    video_start_time = time.time()
    # remove the above line if you want to pass Id seperately
    ID = name
    if ID == None:
        ID = random.getrandbits(32)

        # for frinting video on multiple regions(max 2 supported )
    # detect how many different frames regions are listed in regions
    crop_original = 0
    crop_region = []
    if len(regions) == 1:
        crop_original = 1
    elif len(regions) == 2:
        crop_original = 1
        crop_region = regions[1]
    elif len(regions) == 0:
        crop_original = 0
    else:
        return "more than two regions feature is not added yet,please check regions parameter"
    if crop_original:
        if len(regions[0]) == 4 or (len(regions) > 1 and len(regions[1]) == 4):
            pass
        else:
            return "please pass correct measures in regions parameter"

    # circular buffer above is used typically in streaming applications
    frames = None
    pdq_hashes = None
    qualities = None
    timestamps = []
    cnt = 0
    # for arg in argv:
    cap = cv2.VideoCapture(file)

    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
    channels = cap.get(cv2.CAP_PROP_CHANNEL)
    image_size = int(width) * int(height) * int(channels)
    buff = RingBuffer(capacity=max_qlength, dtype=np.object)  # intializing a ringbuffer
    prev_hv = None

    pdq_type = []
    pdq_change = []
    frames = []
    frames_img = []
    pdq_hashes = []
    qualities = []
    timestamps = []
    region_type = []
    frame_class = []
    last_frames_size = 0
    frames_size = 0
    fps = cap.get(cv2.CAP_PROP_FPS)  # frames for second
    time_start = cap.get(cv2.CAP_PROP_POS_MSEC)
    # total = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))  # total Number of frames
    total = fps * total_duration
    # print ("total frames", total)
    try:
        while (cap.isOpened()):
            ret, frame = cap.read()
            frame_orig = frame
            # if we have multiple regions taking the first one
            if crop_original == 1:
                # select original / main region
                frame_orig = frame[regions[0][0]:regions[0][0] + regions[0][2],
                             regions[0][1]:regions[0][1] + regions[0][3]]

            # maintain a count for recent frames of size sliding_window
            # ie evaluate min and max PDQ diff. for frames in every sliding_window group
            # if DEBUG == 1:
            # print ("len buff", len(buff))
            # if cnt%50==0:
            # print(frame_orig.shape)
            # cv2_imshow(frame_orig)

            if DEBUG and cnt >= total - 1:
                print("cnt", cnt)
            if len(buff) >= sliding_win or (cnt >= total - 1 and cnt <= total + 1):
                if DEBUG and len(buff) != sliding_win:
                    print("cnt", cnt)
                buff_temp = copy.deepcopy(buff)
                prev_hv = calcs(buff_temp, buff, cap, ID, cnt - sliding_win, sliding_win, frames, pdq_hashes, qualities,
                                pdq_type, pdq_change,
                                crop_region, frame_class, region_type, prev_hv)

                frames_size = len(frames)
                # print("len frames, start_fr", frames_size, cnt-sliding_win)

                for pos in range(last_frames_size, frames_size):
                    frames_img.append(buff_temp[frames[pos] - (cnt - sliding_win)])
                    timestamps.append(int(time_start + frames[pos] * 1000 / float(fps)))
                    # print ("frame num, timestamp", frames[pos] , int(time_start + frames[pos] * 1000/float(fps)) )

                while (len(buff) > 0):
                    buff.popleft()

                # save. new frame after processing previous of size sliding_win
                # use cropped frame_orig instead of frame
                save_frame_ring(buff, frame_orig, max_qlen)
                last_frames_size = frames_size

            else:
                # print ("E.len buff", len(buff))
                save_frame_ring(buff, frame_orig, max_qlen)
                # print ("E2. len buff", len(buff))

            if cnt >= total:
                break
            cnt += 1
    except TypeError:
        pass
    except Exception as err:
        print(err)
    # write parquet for. each region type
    # print("lenght of \npdq_hashes",len(pdq_hashes),"\ntimestamps",len(timestamps),"\npdq type",len(pdq_type),"\nframe_class",len(frame_class),"\nregion_type",len(region_type))
    # write the output to files and saving frames if save_img==True
    write_parquet(qualities, pdq_hashes, frames_img, pdq_type, pdq_change, timestamps, wpath, name, ref, ID,
                  frame_class, region_type, save_img)

    # closing the video
    cv2.destroyAllWindows()
    cap.release()
    print("Time taken for video segmentation and fingerprinting is:", time.time() - video_start_time)
    # return frames_img,pdq_hashes,qualities,pdq_type, timestamps,ID


# audio segmentation training
def train_hmm(data_path, win, step, fs, hmmModel):
    """
    arguments:-
        data_path: Path of Directory of the Data to train
        win: window to get features(441->10msec)
        step: overlap (step=441 means window to jump)if win=4410(100msec),step=2205(50msec) then features are calculated with 50% overlap
        fs:sampling rate
        hmmModel:name to save the trained Hmm model

    retuns:
        A message
          Suceess or failure
    function:-
        Trains a Hmm model for audio segmentation

    """
    try:
        start_time = time.time()
        # getting  all wav files in the folder
        files = []
        for i, (dirpath, dirnames, filenames) in enumerate(os.walk(data_path)):
            if (dirpath is not data_path):
                for f in filenames:
                    if ('.wav' in f):
                        files.append(os.path.join(dirpath, f))
        # deriving the necessary features and labels
        features = np.empty((68, 0))
        ind = 0
        labels = np.empty((1, 0))
        for file in files:
            signal, fs = librosa.load(file, sr=fs)
            feat, feat_names = STF.feature_extraction(signal, sampling_rate=fs,
                                                      window=win, step=step, deltas=True)
            # features - (#feats x #frames)
            features = np.hstack((features, feat))
            labels = np.hstack((labels, ind * np.ones((1, feat.shape[1]))))
            # labels = np.hstack((labels,ind*np.ones((1,features.shape[1]))))
            ind += 1
        # training hmm based model
        cl_priors, trans_mat, means, cov = AS.train_hmm_compute_statistics(features, labels.T)
        hmm = hmmlearn.hmm.GaussianHMM(cl_priors.shape[0], "diag")
        hmm.covars_ = cov
        hmm.means_ = means
        hmm.startprob_ = cl_priors
        hmm.transmat_ = trans_mat

        AS.save_hmm(hmmModel, hmm, np.unique(labels).tolist(), win, step)
        print('time_taken: ' + str(time.time() - start_time))
        return "Model trained"
    except Exception as er:
        print("error occured while training the model check error messgae", er)
        return er


# import edit_pyaudio as ep
def analyse_audio(signal, orig_num_channels, name, wpath, dt=2,
                  model="Pretrained_Models/hmmSample4",
                  timestamp_start_time=0,
                  ID=None, debug=False, is_split=False,
                  window1=1000, window2=2000):
    """
    arguments:-
          signal: 1D array containing the audio to process; it should be mono,
      44.1 kHz, floating point, with values in [-1.0, 1.0]
      orig_num_channels: number of channels in the original audio from which
      signal array was converted; used as a key/value pair in the output
      name: Name of the file (used to for naming output files)
      wpath: Output directory to write the frames,parquet and tsv files
      dt=t-i th feature used to compute fingerprint
      model: path of the trained hmm model
      Id: A unique number given to Identify files
      debug: Boolean
                  True: Outputs the Features and all ouptus to tsv
                  False: Prints the time taken and stores only relevent information
      is_split: Boolean
                  True:Takes the  timestamp_start_time and writes the timestammps accordingly
                  False:Considers the timestamp_start_time=0
      timestamp_start_time:Timestamp start values (=0 for single files =something if is_split is true)
      window1: time window in milliseconds to do window-level segmentation
      window2: same as window1, window2 output is stored in final dict

  returns:
    At the moment, only if failure; see below
  function:
    Generates the features, fingerprints and predicts labels;
    also calls "write_audio_parquet" to save the output to files
    """

    try:
        # remove the below line if you want to pass Id seperately
        ID = name
        start_time = time.time()
        # loading  the model from the given path
        with open(model, 'rb') as f:
            hmm = cp.load(f)
            classes = cp.load(f)

        # Note that the file-reading code from the original version
        # is deleted here.  The Numpy array "signal" with the audio
        # samples is passed to this method.

        # Feature extraction
        # we get 68 rows and n columns we can use slicing to get particular features to get only 34(no deltas) use deltas=False in argument, in function

        # to get b_450(own values for mfcc computation) use below line
        # features,_ = ep.feature_extraction(signal, 44100,17640,4410)

        # to get d_450(default values for features calculation) 44100->sampling rate,17640-->window,4410-->step(overlap)
        features, features_names = STF.feature_extraction(signal, 44100, 17640, 4410)

        # to get only 13 Mel frequency features
        # features=features[8:21][:]

        # predicting the segments
        fts = features.T
        labels = hmm.predict(fts)
        # labels to segments
        segs, classes = AS.labels_to_segments(labels, 4410)
        # labels for 2 window sizes using above features(considers window range features as enitre audio features and calclates labels)
        window1_labels = []
        window2_labels = []
        step_w1 = window1 // 100
        for start in range(0, int(fts.shape[0]), step_w1):
            labels_w1 = hmm.predict(fts[start:start + step_w1, :])
            window1_labels.extend(labels_w1.tolist())

        # labels for window 2
        step_w2 = window2 // 100
        for start in range(0, int(fts.shape[0]), step_w2):
            labels_w2 = hmm.predict(fts[start:start + step_w2, :])
            window2_labels.extend(labels_w2.tolist())

        # Generating Fingerprints
        fingerprints = []
        # for first features assign its sign as fingerprint
        # check first features sign
        for i in range(dt):
            fins = ""
            for k in range(13):
                if (fts[i][8:22][k] > 0):
                    fins = fins + "1"
                else:
                    fins = fins + "0"
            fbit = hex(int(fins, base=2))
            fbit = '0x' + fbit[2:].zfill(4)
            fingerprints.append(fbit)

        # for each feature subtract j-dt feature and check sign to assingn fingerprints
        for j in range(0, fts.shape[0] - dt):
            fins = ""
            # for each mfcc feature
            for k in range(13):
                if (fts[j + dt][8:22][k] - fts[j][8:22][k] > 0):
                    fins = fins + "1"
                else:
                    fins = fins + "0"
            fbit = hex(int(fins, base=2))
            fbit = '0x' + fbit[2:].zfill(4)
            fingerprints.append(fbit)

        # a Dictionary to store all the values
        final_dict = {}

        # The code using ffprobe to detect the number of channels
        # in the original file is deleted here.  We pass in a
        # parameter orig_num_channels.

        final_dict['channels'] = orig_num_channels
        final_dict['features_names'] = features_names
        final_dict['labels'] = labels
        final_dict['window1_labels'] = window1_labels
        final_dict['window2_labels'] = window2_labels
        final_dict['segments'] = segs

        # convert silence range to 0-100
        silence_score = []
        volume = []
        for energy in features[1, :]:
            volume.append(int(10.0 * (math.log(energy, 10) - 3.343408593803857)))
            if energy < 0.00221:
                silence_score.append(1)
            else:
                silence_score.append(0)

        final_dict['silence'] = silence_score
        final_dict['volume'] = volume
        final_dict['features'] = fts
        final_dict['fingerprints'] = fingerprints

        # adding timestamps checking if its a split so that it can continue from split time
        if is_split:
            # total_time=features.shape[1]*((1/441)*mfcc_step*10)
            timestamps = []
            for i in range(int(timestamp_start_time), int((features.shape[1] * 100) + timestamp_start_time), 100):
                # start=400 beacuse window=400msec and step=100  above because of the step used for features calculation
                timestamps.append(400 + i)
        else:
            timestamps = []
            for i in range(0, int(features.shape[1] * (100)), 100):
                timestamps.append(400 + i)

        final_dict['timestamps'] = timestamps
        # storing the results into tsv and praquet file
        write_audio_parquet(final_dict, wpath, name, ID, debug)
        # pr=pd.read_parquet(pz_file)

        print('time_taken for audio segmentation and fingerprinting is : ' + str(time.time() - start_time))
        # return final_dict
        # Uncommented this return to have a return code with success
        return "Done"
    except Exception as er:
        return er
