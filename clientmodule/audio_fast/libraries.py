# libraries for video file

import os
import cv2
import csv
import copy
import time
import base64
import random
import pdqhash
import traceback
import numpy as np
import pandas as pd
from datetime import datetime
from numpy_ringbuffer import RingBuffer

# from google.colab.patches import cv2_imshow
# import boto3

# importing libraries for audio fingerprinting

# import os
# import time
import shutil
import hmmlearn
# import numpy as np
# import pandas as pd
import cloudpickle as cp  # to load the model

import librosa  # to read the audio file
#import pyAudioAnalysis.ShortTermFeatures as STF
#import ShortTermFeatures as STF
#import edit_pyaudio as STF
import chroma_pyaudio as STF
#import fast_pyaudio as STF

import pyAudioAnalysis.audioSegmentation as AS

from pathlib import Path  # to parse path
import subprocess
from subprocess import check_call, Popen, PIPE  # to run bash commands
import ntpath  # to get file name from path

# import edit_pyaudio as ep

# libraries for writing output to files


# libraries for splitting
import re
import math
import shlex
