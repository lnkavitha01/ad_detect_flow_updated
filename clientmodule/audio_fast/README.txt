Running Timing Tests on analyse_audio()

Peter Wendt

The aim is to run timing tests on the analyse_audio in main.py.  Most of the
CPU time is spent in librosa.load() and STF.feature_extraction().  The
original code timed the entire analyse_audio() method, and I have added
timing around the two called methods.

A few "environment" details ... Make sure that all the rewuirements are
installed.  If something is not installed, python or python3 will inform
you when you try to import the local python module(s).  Also, paths in the
local code have neen changed so that this code works independently in this
directory on Linux, rather than in Colab on your Google Drive.

I have included input files in the Media subdirectory.

cat_ad.mp4 = 30-second video, audio at 44100 Hz.  This will require no extra
processing inside librosa.load().

BLOOM_Nsec.wav = audio from the BLOOM mp4 video; N=60, 10, or 1.
x These have audio at 16000 Hz, so require lot of processing from
librosa.load() to convert it to 44100 Hz.

There are THREE versions of the code you can run. 

main.py The standard code; processes audio at 44.1 kHz.
main_mod.py The code that processes the audio at its native sample rate
main_fast.py The fast code; processes audio at 44.1 kHz; only computes
MFCC and energy; bugs in segmentation and writing the output file,
but OK for CPU timing.

To run the standard code with cat_ad.mp4 ...

At the Linux command prompt, type "python" or "python3", depending on which
version of Python you use.

At the Python prompt, type "import main"

If there are no errors (not warnings), type

main.analyse_audio("./Media/cat_ad.mp4", cat_ad, "./fingerprints")

and wait.

The first parameter is the input file, the second is the base name and
subdiretory for the output fingerprints, and the third is the base directory
for the fingerprints.

You will find the fingerprints in

./fingerprints/pdq/cat_ad


Anyway, when the method has finished, you will see CPU times for the two
methods and analyse_audio itself.

To run the same code with BLOOM_60sec.wav, type

main.analyse_audio("./Media/BLOOM_60sec.wav", BLOOM, "./fingerprints")

This should show much higher relative CPU time in librosa.load().

Now, to test with my modified code with all the ferastures ...

exit python or python 3 (control-D or "quit()")

enter it again (type python or python3 at the Linux prompt)

type "import main_mod"

to run the code, type

main_mod.analyse_audio()

with the same parameter lists as before

To use my very fastest code with only the energy and MFCC featyres computed,
substitute "main_fast" for "main" or "main_mod".

The fingerprints SHOULD be very close to the old fingerprints.  Theey are saved
with time/datestamps.  The CPU times will be much smaller.
